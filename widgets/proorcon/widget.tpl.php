<?php

/**
 * @file
 * widget.tpl.php
 *
 * proorcon widget theme for Vote Up/Down
 */
?>
<div class="vud-widget vud-widget-proorcon" id="<?php print $id; ?>">
  <?php if ($show_links): ?>
  
  <table>
	  <tr>
<td>
  <!-- CON --> 
    <?php if ($show_up_as_link): ?>
      <a href="<?php print $link_up; ?>" rel="nofollow" class="<?php print $link_class_up; ?>">
    <?php endif; ?>
        <div class="<?php print $class_up; ?> proorcon-up" title="<?php print t('Against !'); ?>">Against !</div>
        <div class="element-invisible"><?php print t('Against !'); ?></div>
    <?php if ($show_up_as_link): ?>
      </a>
    <?php endif; ?>
  </td>
  <td>
  <!-- LABEL -->
  <div class="proorcon-score">
    <span class="proorcon-current-score"><?php print $unsigned_points; ?></span>
    <?php print $vote_label; ?>
  </div>
  </td>
  <td>
  <!-- PRO --> 
    <?php if ($show_down_as_link): ?>
      <a href="<?php print $link_down; ?>" rel="nofollow" class="<?php print $link_class_down; ?>">
    <?php endif; ?>
        <div class="<?php print $class_down; ?> proorcon-down" title="<?php print t('In favour !'); ?>">In favour !</div>
        <div class="element-invisible"><?php print t('In favour !'); ?></div>
    <?php if ($show_down_as_link): ?>
      </a>
    <?php endif; ?>
  </td>
  </tr>
  </table>
    
  <?php endif; ?>
</div>
